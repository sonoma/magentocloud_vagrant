#!/bin/bash

echo "Copy nginx setup"
    sudo cp /home/vagrant/magento2 /etc/nginx/sites-available/magento2
    sudo cp /home/vagrant/nginx /etc/init.d/nginx
    sudo chmod +x /etc/init.d/nginx
    sudo /usr/sbin/update-rc.d -f nginx defaults

    if [ ! -L /etc/nginx/sites-enabled/magento2 ]; then
      sudo ln -s /etc/nginx/sites-available/magento2 /etc/nginx/sites-enabled/magento2
    fi

    sudo service nginx restart
    sudo service php7.2-fpm restart