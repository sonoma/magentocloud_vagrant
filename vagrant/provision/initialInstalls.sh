#!/bin/bash

echo "Provisioning virtual machine..."

echo "Installing Nginx"
    apt-get install nginx -y

echo "Installing Redis"
    sudo apt-get -y install redis-server

echo "Updating PHP repository"
    sudo apt-get install python-software-properties build-essential -y
    sudo add-apt-repository ppa:ondrej/php -y
    sudo apt-get update

echo "Installing PHP"
    apt-get install php7.2-common php7.2-dev php7.2-cli php7.2-fpm -y
    sudo apt-get install -y libapache2-mod-php7.2 php7.2-common php7.2-gd php7.2-mysql php7.2-curl php7.2-intl php7.2-xsl php7.2-mbstring php7.2-zip php7.2-bcmath php7.2-iconv php7.2-soap
    sudo service apache2 stop
    sudo update-rc.d apache2 disable

echo "Installing mysql"
    sudo apt-get update
    sudo debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root_password password root'
    sudo debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root_password_again password root'
    sudo apt-get -y install mysql-server-5.6

    sudo apt-get install php7.2-mysqlnd mysql-client-core-5.6 mysql-client-5.6 -y

    mysql -uroot -proot -e 'CREATE DATABASE IF NOT EXISTS `amorelie-cloud` CHARACTER SET utf8 COLLATE utf8_general_ci'


echo "Installing XDebug"

    sudo apt-get install php7.2-xdebug -y

    sed -i '$ a xdebug.remote_enable=1' /etc/php/7.2/mods-available/xdebug.ini
    sed -i '$ a xdebug.remote_host=192.168.17.1' /etc/php/7.2/mods-available/xdebug.ini
    sed -i '$ a xdebug.remote_connect_back=1' /etc/php/7.2/mods-available/xdebug.ini
    sed -i '$ a xdebug.remote_port=9000' /etc/php/7.2/mods-available/xdebug.ini
    sed -i '$ a xdebug.idekey=PHPSTORM' /etc/php/7.2/mods-available/xdebug.ini

echo "Update php.ini"
    cd /etc/php/7.2/fpm
    sudo cp php.ini php.ini.back
    sudo sed -i "s/^\(memory_limit\).*/\1 $(eval echo = 2G)/" php.ini
    sudo sed -i "s/^\(zlib.output_compression\).*/\1 $(eval echo = On)/" php.ini
    sudo sed -i "s/^\(cgi.fix_pathinfo\).*/\1 $(eval echo = 0)/" php.ini
    sudo sed -i "s/^\(max_execution_time\).*/\1 $(eval echo = 1800)/" php.ini

    cd /etc/php/7.2/cli
    sudo cp php.ini php.ini.back
    sudo sed -i "s/^\(memory_limit\).*/\1 $(eval echo = 2G)/" php.ini
    sudo sed -i "s/^\(zlib.output_compression\).*/\1 $(eval echo = On)/" php.ini
    sudo sed -i "s/^\(max_execution_time\).*/\1 $(eval echo = 1800)/" php.ini
