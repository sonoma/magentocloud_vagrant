# Amorelie Cloud Vagrant machine

## Installing

###Create working directory
Choose whatever you like, eg. `mkdir ~/magento2`. Go to new directory ```cd ~/magento2```

###Clone repository
```
    git clone git@bitbucket.org:sonoma/magentocloud_vagrant.git .
```

###Add your cloud credentials 
Follow the [link](https://devdocs.magento.com/guides/v2.3/install-gde/prereq/connect-auth.html) to find out how you can get your credentials. 
Add them to the file: 
`   nano vagrant/prepared/auth.json`
Use `public key` as `username` and `private key` as `password`

###Modify hosts file: 
        
    192.168.17.19 de.amorelie.local
    192.168.17.19 at.amorelie.local
    192.168.17.19 ch.amorelie.local
    192.168.17.19 fr.amorelie.local
    192.168.17.19 be.amorelie.local
    192.168.17.19 fr-ch.amorelie.local
    192.168.17.19 b2b-de.amorelie.local
    192.168.17.19 b2b-en.amorelie.local
    192.168.17.19 returns.lovella-shop.local
On Mac host file is usually locatad in /private/etc/ directory

### Generate ssh keys
Generate ssh key if you don't have any or if you want to use a custom one for this project. It's important to create and 
use a new one ****if you have worked on other Magento Cloud project before**** using this computer. 
``` 
    ssh-keygen -t rsa
```

###Add public key to bitbucket 

Go to https://bitbucket.org -> View profile -> settings -> security -> SSH Keys

###Run script using default SSH Keys

```
    cd vagrant
    ./install.sh
```

###Run script using custom SSH Keys
As default script uses id_rsa key. If you want to use custom key you need to pass the file name to the install script
```
    cd vagrant
    ./install.sh --ssh_key=my_key_name
```
where my_key_name is a file name you has chosen during ssh-keygen. Available keys could be checked with `ls ~/.ssh`


It might take up to 1 hour. Provide you (host) user password when prompted.

###Check the result 
        
Go to [http://de.amorelie.local/](http://de.amorelie.local/) and verify everything works.


###Additional notes

New database will be be installed every time you run ```vagrant provision``` or ``` vagrant up --provision```. If you want to avoid this please comment out line 26 in Vagrantfile 